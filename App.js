import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NativeRouter, Route, Link, Switch } from "react-router-native";

//component
import HomeComponent from './components/HomeComponent'
import LoginComponent from './components/LoginComponent'
import RegisterComponent from './components/RegisterComponent'

export default App = () => {
  return (
    <View style={styles.container}>
      <NativeRouter>
        <Switch>
          <Route exact path='/' component={HomeComponent} />
          <Route exact path='/login' component={LoginComponent} />
          <Route exact path='/register' component={RegisterComponent} />
        </Switch>
      </NativeRouter>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
