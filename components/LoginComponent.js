import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export default LoginComponent = ({history}) => {
    return (
        <View>
            <Text style={styles.text_login}>Login</Text>
            <Button title="back" onPress={ () => history.push('/') }/>
        </View>
    )
}

const styles = StyleSheet.create({
    text_login: {
      color: 'red',
      fontSize: 36,
    },
  });