import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default HomeComponent = ({history}) => {
    return (
        <View>
            <Button title='Home' onPress={ () => history.push('/') }/>
            <Button title='Login' onPress={ () => history.push('/login') }/>
            <Button title='Register' onPress={ () => history.push('/register') }/>
        </View>
    )
}